#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Ejercicio Programaci�n.
typedef struct{
	int o1;
	int o2;
	int o3;
	int o4;
}octeto;

typedef struct{
	char linea[50];
	octeto ip;
	octeto mascara;
	octeto red;
	int tamMascara;
}nodo;


int contarLineasFichero();
void quitarSaltosLinea(char linea[]);
void limpiarString(char linea[]);
void leerFichero(nodo vNodos[]);
void calcularMascara(nodo vNodos[],int tam);

int main(int argc, char *argv[]) {
	int i,numLineas;
	
	numLineas = contarLineasFichero();
	nodo vNodos[numLineas]; //Vector con el n�mero de lineas que se van a leer
	
	leerFichero(vNodos);
	calcularMascara(vNodos,numLineas);
	
	
	
for(i=0;i<numLineas;i++){
		printf("Linea leida: %s \nip: %i.%i.%i.%i  - mascara: %i.%i.%i.%i \n -------------\n\n", 
		vNodos[i].linea, 
		vNodos[i].ip.o1,vNodos[i].ip.o2,vNodos[i].ip.o3,vNodos[i].ip.o4,
		vNodos[i].mascara.o1,vNodos[i].mascara.o2,vNodos[i].mascara.o3,vNodos[i].mascara.o4);
	}
	
	
	return 0;
}

void calcularMascara(nodo vNodos[], int tam){
	int i,j,aux,max = 255;
	int resto, cociente, octeto, mask;
	
	for (i =0; i<tam; i++){
		octeto=1;
		aux = vNodos[i].tamMascara;
		
		//Relleno los octetos enteros con 255
		cociente = aux/8;
		for (j=1; j<=cociente; j++){	
			switch (j){
				case 1:
					vNodos[i].mascara.o1 = 255;
					break;
				case 2:
					vNodos[i].mascara.o2 = 255;
					break;
				case 3:
					vNodos[i].mascara.o3 = 255;
					break;
				case 4:
					vNodos[i].mascara.o4 = 255;
					break;				
			}
			octeto++;
		}
		
		//Relleno el octeto que tiene bits de red y de host
		resto = aux%8;
		if (resto>0){
			switch (octeto){
				case 1:
					mask = 255 >> resto;
					mask = mask ^ 255;
					vNodos[i].mascara.o1 = mask;
					break;
				case 2:
					mask = 255 >> resto;
					mask = mask ^ 255;
					vNodos[i].mascara.o2 = mask;
					break;
				case 3:
					mask = 255 >> resto;
					mask = mask ^ 255;
					vNodos[i].mascara.o3 = mask;
					break;
				case 4:
					mask = 255 >> resto;
					mask = mask ^ 255;
					vNodos[i].mascara.o4 = mask;
					break;				
			}
			octeto++;
		}
		
		//Completo los octetos con 0 que forman parte del hosts
		for (j=octeto; j<=4; j++){
				switch (j){
				case 1:
					vNodos[i].mascara.o1 = 0;
					break;
				case 2:
					vNodos[i].mascara.o2 = 0;
					break;
				case 3:
					vNodos[i].mascara.o3 = 0;
					break;
				case 4:
					vNodos[i].mascara.o4 = 0;
					break;				
			}
		}
	}
	
}

//Leer del fichero la linea leida, y guarda la ip y el tama�o de la mascara por cada linea
void leerFichero(nodo vNodos[]){
	int i=0,j=0,m=0,octeto=1;
	char aux[3];
	char linea[20];
	char c = '0';
	FILE *f = fopen("direcciones.txt","r");
	if (f==NULL){
		printf("Fichero no abierto");
	}
	//Guardar las lineas leidas
	for(i=0;(!feof(f));i++){
		limpiarString(linea);
		fgets(vNodos[i].linea,20,f);
		quitarSaltosLinea(vNodos[i].linea);
	}
	
	rewind(f); //Apuntador de fichero al principio
	
	
	for (i=0; !feof(f); i++){ //i marca la linea en la que estamos y el nodo
		limpiarString(aux);
		c='0';
		octeto=1;
		m=0;
		for (j=0; c!='/';j++){ // j marca el caracter
			c = fgetc(f);
			if (c!='.' && c!= '/'){
				aux[m] = c;
				m++;
			}else{
				if (c=='.'){
					switch (octeto){
						case 1:
							vNodos[i].ip.o1 = atoi(aux);
							break;
						case 2:
							vNodos[i].ip.o2 = atoi(aux);
							break;
						case 3:
							vNodos[i].ip.o3 = atoi(aux);
							break;
					}
					octeto++;
					m=0;
					limpiarString(aux);
				}
				if (c=='/'){
					vNodos[i].ip.o4 = atoi(aux);
					limpiarString(aux);
					fgets(aux,4,f);
					vNodos[i].tamMascara = atoi(aux);
				}
			}
		}
	
		
	}
	
	
	fclose(f);
}

void limpiarString(char linea[]){
	int i;
	for (i=0;i<3;i++){
		linea[i]='\0';
	}
}

void quitarSaltosLinea(char linea[]){
	int i;
	for (i =0; i<20; i++){
		if (linea[i] == '\n'){
			linea[i] = '\0';
			break;
		}
	}
}

int contarLineasFichero(){
	int cont=0;
	char linea[50];
	
	FILE *f = fopen("direcciones.txt","r");
	while(!feof(f)){
		fgets(linea,50,f);
		cont++;
	}
	fclose(f);
	return cont;
}
